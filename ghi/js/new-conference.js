window.addEventListener("DOMContentLoaded", async ()=> {
const url = "http://localhost:8000/api/locations/";
try{
    const response = await fetch(url);
    if (response.ok){
    const data = await response.json();
    const locationTag = document.getElementById("location");
    for (let location of data.locations){
        const option = document.createElement("option");
        option.value = location.id;
        option.innerHTML = location.name;
        locationTag.appendChild(option);
    }
    }else console.error("Response not OK")
}
catch(e){console.error("Issue with the fetch")}

const formTag = document.getElementById("create-conference-form");
formTag.addEventListener("submit", async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const fetchConfig = {
        method: "POST",
        body: json,
        headers: {'Content-Type':'aplication/json'}
    }

    const confUrl = " http://localhost:8000/api/conferences/";
    const createResponse = await fetch(confUrl,fetchConfig);
    if(createResponse.ok){
        formTag.reset();
    }





})

})
