function createCard(name, description, pictureUrl, dates, location) {
    return `
      <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
        ${dates}
        </div>
      </div>
    `;
  }



window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";
    try{
    const response = await fetch(url);
    if (response.ok){
        let data = await response.json();
        // console.log(data)

        for(let index=0; index<data.conferences.length; index++){
        let conference = data.conferences[index];
        let detailUrl = `http://localhost:8000${conference.href}`;
        let detailResponse = await fetch(detailUrl);
        if(detailResponse.ok){
            let details = await detailResponse.json();
            // console.log(details)
            let title = details.conference.name;
            let descriptions = details.conference.description;
            let start = new Date(details.conference.starts)
            let end = new Date(details.conference.ends)

            let startEnd = `${start.toLocaleDateString()} - ${end.toLocaleDateString()}`
            let pictureUrl = details.conference.location.picture_url;
            let loc = details.conference.location.city
            // console.log(loc)

            let html = createCard(title,descriptions,pictureUrl,startEnd,loc);

            let col_index = index % 3;
            let column = document.querySelectorAll('.col');
            column[col_index].innerHTML += html;

        }
    }

    } else{
        console.error("Status not OK");
    }

    }catch(e){
        console.error("Error fetching url.");
    }

});
