window.addEventListener("DOMContentLoaded",async () =>{
    const url = " http://localhost:8000/api/conferences/";
    try{
    const response = await fetch(url);
    if(response.ok){
        const data = await response.json();
        const cofTag = document.getElementById("conference");
        for(let conf of data.conferences){
            const option = document.createElement("option");
            option.value = conf.id;
            option.innerHTML = conf.name;
            cofTag.appendChild(option);
            }

        }else console.error("I'm not ok")

    }catch(e){
    console.error("nope")
    }

    const formTag = document.getElementById("create-presentaion-form");
    formTag.addEventListener("submit", async event =>{
        event.preventDefault();
        const selectTag = document.getElementById("conference");
        let selected = selectTag.options[selectTag.selectedIndex].value;
        let pUrl = `http://localhost:8000/api/conferences/${selected}/presentations/`;
        let formData = new FormData(formTag);
        let json = JSON.stringify(Object.fromEntries(formData));

        const fetchConfig = {
            method: "POST",
            body: json,
            headers:{'Content-Type': 'application/json'}
        }

        const createRespone = await fetch(pUrl,fetchConfig);
        if(createRespone.ok){
            formTag.reset();
            let testing = await createRespone.json()
            console.log(testing)

        }
    })




})
