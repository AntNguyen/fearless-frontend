import { useState, useEffect } from "react";

function ConferenceForm (){
    const [name,setName] = useState("")
    const [start,setStart] = useState("")
    const [end,setEnd] = useState("")
    const [presentation,setPresentaion] = useState(0)
    const [attendee,setAttendee] = useState(0)
    const [location,setLocation] = useState("")
    const [description,setDescription] = useState("")
    const nameChange = (event)=> {setName(event.target.value)}
    const startChange = (event)=> {setStart(event.target.value)}
    const endChange = (event)=> {setEnd(event.target.value)}
    const presentationChange = (event)=> {setPresentaion(event.target.value)}
    const attendeesChange = (event)=> {setAttendee(event.target.value)}
    const locationChange = (event)=> {setLocation(event.target.value)}
    const descriptionChange = (event)=> {setDescription(event.target.value)}

    const handleSubmit = async(event) =>{
        event.preventDefault()

        const data ={}
        data.name = name
        data.starts = start
        data.ends = end
        data.max_presentations = presentation
        data.max_attendees = attendee
        data.location = location
        data.description = description

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {'Content-Type': 'application/json'}
        }

        let response = await fetch("http://localhost:8000/api/conferences/",fetchConfig)
        if(response.ok){
            const confData = await response.json();
            // console.log(confData);

            setName("")
            setStart("")
            setEnd("")
            setPresentaion(0)
            setAttendee(0)
            setLocation("")
            setDescription("")

        }
    }

    const [locations,setLocations] = useState([])
    const fetchData = async()=>{
        const locUrl = "http://localhost:8000/api/locations/";
        const response = await fetch(locUrl);
        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);

        }
    };

    useEffect(()=>{
        fetchData()
    },[]);

    return(

    <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Conference</h1>
                <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input onChange={nameChange} value={name} placeholder="Name" required type="text" name = "name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={startChange} value={start} placeholder="Starts" required type="date" name = "starts" id="start" className="form-control"/>
                    <label htmlFor="start">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={endChange} value={end} placeholder="Ends" required type="date" name = "ends" id="end" className="form-control"/>
                    <label htmlFor="end">Ends</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description">Description</label>
                    <textarea onChange={descriptionChange} value={description} className="form-control" id="description" name = "description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={presentationChange} value={presentation} placeholder="Max  Presentations" required type="number" name = "max_presentations" id="presentations" className="form-control"/>
                    <label htmlFor="presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={attendeesChange} value={attendee}placeholder="Max  attendees" required type="number" name = "max_attendees" id="attendees" className="form-control"/>
                    <label htmlFor="attendees">Maximum attendees</label>
                </div>
                <div className="mb-3">
                    <select onChange={locationChange} value={location} required id="location" name = "location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                    return (
                        <option key= {location.id} value={location.id}>{location.name}</option>
                    );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>

    );

}


export default ConferenceForm
