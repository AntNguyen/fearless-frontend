import { useEffect, useState  } from "react";

function PresentationForm(){

  const [name,setName] = useState("");
  const [email,setEmail] = useState("");
  const [company,setCompany] = useState("");
  const [title,setTitle] = useState("");
  const [synopsis,setSynopsis] = useState("");
  const [conference,setConference] = useState("");
  const nameChange = (event)=>{setName(event.target.value)};
  const emailChange = (event)=>{setEmail(event.target.value)};
  const companyChange = (event)=>{setCompany(event.target.value)};
  const titleChange = (event)=>{setTitle(event.target.value)};
  const synopsisChange = (event)=>{setSynopsis(event.target.value)};
  const conferenceChange = (event)=>{setConference(event.target.value)};

  const handleSubmit = async(event)=>{
    event.preventDefault();
    const data = {};
    data.presenter_name = name;
    data.presenter_email = email;
    data.company_name = company;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    const fetchConfig = {
      method:"POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }
    const subUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
    const subResponse = await fetch(subUrl,fetchConfig);
    if(subResponse.ok){
      const testing = await subResponse.json();
      setName("")
      setEmail("")
      setCompany("")
      setTitle("")
      setSynopsis("")
      setConference("")
    }
  }

  const [conferences,setConferences] = useState([])
  const fetchData = async() =>{
    const response = await fetch("http://localhost:8000/api/conferences/");
    if(response.ok){
      const data = await response.json();
      setConferences(data.conferences);
    }
  }

  useEffect(()=>{
    fetchData();
  },[]);


    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentaion-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={nameChange} placeholder="Name" required type="text" name = "presenter_name" id="name" className="form-control"/>
                <label htmlFor="name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={email} onChange={emailChange}placeholder="Presenter email" required type="email" name = "presenter_email" id="email" className="form-control"/>
                <label htmlFor="email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                  <input value={company} onChange={companyChange} placeholder="Company name" type="text" name = "company_name" id="company" className="form-control"/>
                  <label htmlFor="company">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={title} onChange={titleChange} placeholder="Title" required type="text" name = "title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="synopsis">Synopsis</label>
                  <textarea value={synopsis} onChange={synopsisChange} className="form-control" id="synopsis" name = "synopsis" rows="3"></textarea>
                </div>
              <div className="mb-3">
                <select value={conference} onChange={conferenceChange} required id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference=>{
                      return(
                        <option key= {conference.id} value={conference.id}>{conference.name}</option>
                      )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default PresentationForm
